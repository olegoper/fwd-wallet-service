package account

import (
	"coin.io/wallet/common"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
)

// Data layer for accounts management
type DAO interface {
	InsertAccount(tx *gorm.DB, acc *Account) error
	FindAllAccounts(tx *gorm.DB) ([]*Account, error)
	FindAccountByID(tx *gorm.DB, accountID AccountID, forUpdate bool) (*Account, error)
	UpdateBalance(tx *gorm.DB, account *Account, balance Money) (*Account, error)
}

type dao struct {
}

var _ DAO = (*dao)(nil)

func NewDAO() DAO {
	return &dao{}
}

// Inserts new account in DB
func (dao *dao) InsertAccount(tx *gorm.DB, acc *Account) error {
	err := tx.Create(acc).Error
	if err == nil {
		return nil
	}

	if err, ok := err.(*pq.Error); ok && err.Code.Name() == "unique_violation" {
		return common.ErrInvalidArgument.New("Account already exists")
	}
	return common.ErrGeneralError.New(err.Error())
}

func (dao *dao) FindAllAccounts(tx *gorm.DB) ([]*Account, error) {
	var res []*Account
	err := tx.Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}

// Finds account by ID. Optionaly locks record for update
func (dao *dao) FindAccountByID(tx *gorm.DB, accountID AccountID, forUpdate bool) (*Account, error) {
	var res Account

	if forUpdate {
		tx = tx.Set("gorm:query_option", "FOR UPDATE")
	}
	err := tx.Where("id = ?", accountID).First(&res).Error

	if err == nil {
		return &res, nil
	}

	if gorm.IsRecordNotFoundError(err) {
		return nil, common.ErrNotFound.Newf("Account %q not found", accountID)
	}

	return nil, common.ErrGeneralError.Newf("Failed to retrieve account %q", accountID)
}

// Updates account balance to target value
func (dao *dao) UpdateBalance(tx *gorm.DB, account *Account, balance Money) (*Account, error) {
	err := tx.Model(&account).Update("balance", balance).Error
	if err != nil {
		return nil, err
	}

	return account, nil
}
