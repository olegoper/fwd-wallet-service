package dbconf

import (
	"coin.io/wallet/app/account"
	"coin.io/wallet/app/wallet"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

// Initializes DB client and application models
func InitDB(conf *Config) (*gorm.DB, error) {
	db, err := gorm.Open(conf.Dialect, conf.ConnectionOpts)
	if err != nil {
		return nil, err
	}

	err = RegisterModels(db)
	if err != nil {
		db.Close()
		return nil, err
	}

	return db, nil
}

// Returns all registered application models
func GetAppModels() []interface{} {
	models := []interface{}{
		&account.Account{},
		&wallet.Payment{},
	}

	return models
}

// Registers all models in DB
func RegisterModels(db *gorm.DB) error {
	models := GetAppModels()

	for _, m := range models {
		err := db.AutoMigrate(m).Error
		if err != nil {
			return err
		}
	}

	return nil
}
