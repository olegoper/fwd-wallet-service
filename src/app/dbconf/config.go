package dbconf

func NewConfig(dialect string, connectionOpts string) *Config {
	return &Config{
		Dialect:        dialect,
		ConnectionOpts: connectionOpts,
	}
}

// Database config
type Config struct {
	Dialect        string
	ConnectionOpts string
}

// Default DB config for testing
func NewTestConfig() *Config {
	dialect := "postgres"
	connectionOpts := "host=127.0.0.1 port=8543 user=user dbname=coins password=pwd sslmode=disable"
	return NewConfig(dialect, connectionOpts)
}

// Default DB config for production
func NewProdConfig() *Config {
	dialect := "postgres"
	connectionOpts := "host=127.0.0.1 port=8543 user=user dbname=coins password=pwd sslmode=disable"
	return NewConfig(dialect, connectionOpts)
}
