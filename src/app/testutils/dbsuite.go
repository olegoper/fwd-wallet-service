package testutils

import (
	"fmt"

	"coin.io/wallet/app"
	"coin.io/wallet/app/dbconf"
	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/suite"
	"go.uber.org/dig"
)

func BuildContainer(db *gorm.DB) *dig.Container {
	container := dig.New()

	app.Provide(container)

	container.Provide(func() *gorm.DB {
		return db
	})

	return container
}

func PrepareDB() (*gorm.DB, error) {
	conf := dbconf.NewTestConfig()
	return dbconf.InitDB(conf)
}

// Base test suite for testing using DB
type DBTestSuite struct {
	suite.Suite

	DB        *gorm.DB
	Container *dig.Container
}

func (s *DBTestSuite) SetupSuite() {
	db, err := PrepareDB()
	if err != nil {
		panic(fmt.Sprintf("failed to init database %v", err))
	}

	s.Container = BuildContainer(db)
	s.DB = db
}

func (s *DBTestSuite) TearDownSuite() {
	s.DB.Close()
}

func (s *DBTestSuite) DropDB() {
	for _, model := range dbconf.GetAppModels() {
		err := s.DB.Unscoped().Delete(model).Error
		s.Require().NoError(err)
	}
}

func (s *DBTestSuite) SetupTest() {
	s.DropDB()
}
