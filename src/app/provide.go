package app

import (
	"coin.io/wallet/app/account"
	"coin.io/wallet/app/status"
	"coin.io/wallet/app/wallet"
	"go.uber.org/dig"
)

func Provide(container *dig.Container) {
	container.Provide(status.NewService)
	container.Provide(account.NewDAO)
	container.Provide(account.NewService)
	container.Provide(wallet.NewDAO)
	container.Provide(wallet.NewService)
}
