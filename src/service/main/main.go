package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"coin.io/wallet/app"
	"coin.io/wallet/app/dbconf"
	"coin.io/wallet/service"
	"github.com/jinzhu/gorm"
	"go.uber.org/dig"
)

func BuildContainer(db *gorm.DB) *dig.Container {
	container := dig.New()

	app.Provide(container)
	service.Provide(container)

	container.Provide(func() *gorm.DB {
		return db
	})

	return container
}

func PrepareDB() (*gorm.DB, error) {
	conf := dbconf.NewProdConfig()
	return dbconf.InitDB(conf)
}

func main() {
	httpAddr := flag.String("http", ":8080", "http listen address")
	flag.Parse()

	db, err := PrepareDB()
	if err != nil {
		panic(fmt.Sprintf("failed to init database %v", err))
	}
	defer db.Close()

	container := BuildContainer(db)

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	go func() {
		log.Println("app is listening on port:", *httpAddr)
		err := container.Invoke(func(handler http.Handler) {
			errs <- http.ListenAndServe(*httpAddr, handler)
		})
		if err != nil {
			errs <- err
		}
	}()

	log.Fatalln(<-errs)
}
